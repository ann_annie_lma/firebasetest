package com.example.testfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testfirebase.databinding.ActivityMainBinding
import com.example.testfirebase.databinding.ActivityRegistrationBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var auth: FirebaseAuth
    var databaseReference: DatabaseReference? = null
    var database: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        databaseReference = database?.reference!!.child("profile")

        loadUserInfo()
    }

    private fun loadUserInfo()
    {
        val user = auth.currentUser
        val userReference = databaseReference?.child(user?.uid!!)

        binding.tvEmail.text = user?.email
        userReference?.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                binding.tvFirstName.text = snapshot.child("firstname").value.toString()
                binding.tvLastName.text = snapshot.child("lastname").value.toString()

            }

            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

        binding.btnLogOut.setOnClickListener {
            auth.signOut()
            startActivity(Intent(this,LoginActivity::class.java))
        }

        binding.tvUpdateUserInfor.setOnClickListener {
            startActivity(Intent(this,UpdateProfile::class.java))
        }

    }
}