package com.example.testfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.example.testfirebase.databinding.ActivityLoginBinding
import com.example.testfirebase.databinding.ActivityRegistrationBinding
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()

        checkForLogIn()

        logIn()
    }


    private fun logIn()
    {
        binding.btnLogin.setOnClickListener {
            if(TextUtils.isEmpty(binding.etEmail.text.toString()))
            {
                binding.etEmail.error = "Please enter email"
                return@setOnClickListener
            }else if(TextUtils.isEmpty(binding.etPassword.text.toString()))
            {
                binding.etEmail.error = "Please enter password"
                return@setOnClickListener
            }

            auth.signInWithEmailAndPassword(binding.etEmail.text.toString(),
                binding.etPassword.text.toString()).addOnCompleteListener { task ->
                    if(task.isSuccessful)
                    {
                        startActivity(Intent(this,ProfileActivity::class.java))
                        finish()
                    } else
                    {
                        Toast.makeText(this,"Login Failed", Toast.LENGTH_SHORT).show()
                    }
            }
        }

        binding.tvCreateNewAccount.setOnClickListener {
            startActivity(Intent(this,RegistrationActivity::class.java))
        }


    }

    private fun checkForLogIn()
    {
        val currentUser = auth.currentUser
        if(currentUser != null)
        {
            startActivity(Intent(this,ProfileActivity::class.java))
            finish()
        }
    }



}