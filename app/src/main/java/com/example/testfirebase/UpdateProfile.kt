package com.example.testfirebase

import android.R.attr
import android.R.attr.baselineAlignedChildIndex
import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.testfirebase.databinding.ActivityLoginBinding
import com.example.testfirebase.databinding.ActivityUpdateProfileBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import androidx.annotation.NonNull

import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.auth.FirebaseUser

import com.google.firebase.auth.EmailAuthProvider

import com.google.firebase.auth.AuthCredential
import android.R.attr.password
import android.content.Intent


class UpdateProfile : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateProfileBinding
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()

        binding.btnUpdate.setOnClickListener {
            changePassword()

        }
    }



    private fun changePassword() {
        if(binding.etCurrentPassword.text.isNotEmpty() &&
                binding.etNewPassword.text.isNotEmpty() &&
                binding.etRepeatPassword.text.isNotEmpty())
        {
            if(binding.etNewPassword.text.toString().equals(binding.etRepeatPassword.text.toString()))
            {
                val user = auth.currentUser
                if(user != null && user.email != null)
                {
                    val credential = EmailAuthProvider.getCredential(user.email!!,binding.etCurrentPassword.text.toString())

                    user?.reauthenticate(credential).addOnCompleteListener {task ->
                        if(task.isSuccessful)
                        {
                            Toast.makeText(this,"Re-auth success",Toast.LENGTH_SHORT).show()
                            user?.updatePassword(binding.etNewPassword.text.toString()).addOnCompleteListener { task->
                                if(task.isSuccessful)
                                {
                                    Toast.makeText(this,"Your password has updated successfully",Toast.LENGTH_SHORT).show()
                                    auth.signOut()
                                }
                            }
                        } else
                        {
                            Toast.makeText(this,"Re-auth failed",Toast.LENGTH_SHORT).show()
                        }
                    }
                } else
                {
                    startActivity(Intent(this,LoginActivity::class.java))
                }
            } else
            {
                Toast.makeText(this,"Password mismatching",Toast.LENGTH_SHORT).show()
            }

        } else
        {
            Toast.makeText(this,"Please enter all the fields",Toast.LENGTH_SHORT).show()
        }
    }


}