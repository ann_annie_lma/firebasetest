package com.example.testfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.example.testfirebase.databinding.ActivityRegistrationBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegistrationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistrationBinding
    lateinit var auth: FirebaseAuth
    var databaseReference: DatabaseReference? = null
    var database:FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        databaseReference = database?.reference!!.child("profile")

        register()
    }


    private fun register()
    {
        binding.btnRegister.setOnClickListener {

            if(TextUtils.isEmpty(binding.etFirstName.text.toString()))
           {
               binding.etFirstName.error = "Please enter first name"
               return@setOnClickListener
           } else if(TextUtils.isEmpty(binding.etLastName.text.toString()))
           {
               binding.etFirstName.error = "Please enter last name"
               return@setOnClickListener
           } else if(TextUtils.isEmpty(binding.etEmail.text.toString()))
           {
               binding.etFirstName.error = "Please enter email"
               return@setOnClickListener
           } else if(TextUtils.isEmpty(binding.etPassword.text.toString()))
           {
               binding.etFirstName.error = "Please enter password"
               return@setOnClickListener
           }
            auth.createUserWithEmailAndPassword(binding.etEmail.text.toString(),
                binding.etPassword.text.toString()).addOnCompleteListener {task ->
                    if(task.isSuccessful)
                    {

                        val currentUser = auth.currentUser
                        val currentUserDb = databaseReference?.child(currentUser?.uid!!)
                        currentUserDb?.child("firstname")?.setValue(binding.etFirstName.text.toString())
                        currentUserDb?.child("lastname")?.setValue(binding.etLastName.text.toString())

                        Toast.makeText(this,"Registration Success",Toast.LENGTH_SHORT).show()
                        finish()

                    } else
                    {
                        Toast.makeText(this,"Registration Failed",Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }
}